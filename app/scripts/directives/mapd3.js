'use strict';
/**
 * @ngdoc directive
 * @name capacitacionApp.directive:mapd3
 * @description
 * # mapd3
 */
angular.module('capacitacionApp')
  .directive('mapd3', function ($rootScope, mapaFactory) {
    return {
      restrict: 'E',
      //replace: false,
      scope: {
        detalle:'='
      },
      templateUrl: 'views/directives/mapd3.html',
      link: function postLink() {
        console.log('directiva mapd3');
        var width = 960,
            height = 500,
            active = d3.select(null);

        var originTranslate = [-5431,-5367];
        var originZoom = 18;

        var projection = d3.geo.mercator()
          //.scale(500)
          .translate([width / 2, height / 2]);

        var zoom = d3.behavior.zoom()
            .translate(originTranslate)
            .scale(originZoom)
            .scaleExtent([18, 21])
            .on('zoom', zoomed);

        var path = d3.geo.path()
            .projection(projection);

        var svg = d3.select('#map').append('svg')
            .attr('width', width)
            .attr('height', height)
            .on('click', stopped, true);

        svg.append('rect')
            .attr('class', 'background')
            .attr('width', width)
            .attr('height', height)
            .on('click', reset);

        var g = svg.append('g');
        var g2 = svg.append('g');

        svg
            .call(zoom) // delete this line to disable free zooming
            .call(zoom.event);

        //var parametro = { tipo_consulta:'departamentos' };

        /* Solicitar los datos de todos los departamentos del país, luego de los distritos de cada departamento */
        /*var data_departamentos = mapaFactory.getDepartamentos()
          .then(function(data){
            var departamentos = topojson.feature(data, data.objects.visualizacion).features;
            draw(departamentos);
          });*/

        d3.json("data/map-data/topojson_visualizacion.json", function(error, data) {
          if (error) return console.error(error);
          console.log(data);
          var departamentos = topojson.feature(data, data.objects.visualizacion).features;
          draw(departamentos);
        });

        function draw(topo) {
          g.selectAll('path')
              .data(topo)
            .enter().append('path')
              .attr('class', 'feature')
              .attr('d', path)
              .on('click', clicked);

        }

        function distritoClicked(d){
          center(d, this);
        }

        function drawDistritos(distritos) {
          var p = g2.selectAll('path')
            .data(distritos);

            p.exit().remove();
            p.enter().append('path');


            p.attr('class', 'distrito')
              .attr('d', path)
              .on('click', distritoClicked);


        }

        function center(d, sup) {

          if (active.node() === sup) {
            return reset();
          }
          active.classed('active', false);
          active = d3.select(sup).classed('active', true);

          var bounds = path.bounds(d),
              dx = bounds[1][0] - bounds[0][0],
              dy = bounds[1][1] - bounds[0][1],
              x = (bounds[0][0] + bounds[1][0]) / 2,
              y = (bounds[0][1] + bounds[1][1]) / 2,
              scale = 0.9 / Math.max(dx / width, dy / height),
              translate = [width / 2 - scale * x, height / 2 - scale * y];

          svg.transition()
              .duration(750)
              .call(zoom.translate(translate).scale(scale).event);
        }

        function clicked(d) {
          center(d, this);
          var nombreDepto = d.properties.departamento.toLowerCase().replace(' ', '_');
          d3.json("data/map-data/distritos/topojson_" + nombreDepto + ".json", function(error, data) {
            if (error) return console.error(error);
            console.log(data);
            var distritos = topojson.feature(data, data.objects[nombreDepto]).features;
            drawDistritos(distritos);
          });
          /*mapaFactory.getDistritos(d.properties.departamento)
            .then(function (data) {
              console.log(data);
              var distritos = topojson.feature(data, data.objects[nombreDepto]).features;
              console.log('distritos', distritos);
              drawDistritos(distritos);

          });*/
        }

        function reset() {
          //console.log('-------- reset');
          active.classed('active', false);
          active = d3.select(null);
          drawDistritos([]);

          svg.transition()
              .duration(750)
              .call(zoom.translate(originTranslate).scale(originZoom).event);
        }

        function zoomed() {
          g.style('stroke-width', 1.5 / d3.event.scale + 'px');
          g.attr('transform', 'translate(' + d3.event.translate + ')scale(' + d3.event.scale + ')');
          g2.style('stroke-width', 1 / d3.event.scale + 'px');
          g2.attr('transform', 'translate(' + d3.event.translate + ')scale(' + d3.event.scale + ')');
        }

        // If the drag behavior prevents the default click,
        // also stop propagation so we don’t click-to-zoom.
        function stopped() {
          if (d3.event.defaultPrevented) d3.event.stopPropagation();
        }
      }
    };
  });
