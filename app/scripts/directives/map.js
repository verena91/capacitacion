'use strict';

/**
 * @ngdoc function
 * @name capacitacionApp.directive:map
 * @description
 * # map
 * Controller of the capacitacionApp
 */
angular.module('capacitacionApp')
  .directive('mapLeaflet', function (mapaFactory, mapaUtil) {
    return {
      restrict: 'E',
      replace: false,
      scope: {
        detalle:'='
      },
      templateUrl: 'views/directives/map.html',
      link: function postLink(scope, element, attrs) {
            //console.log(L);
        var MECONF = MECONF || {};
        MECONF.tilesLoaded = false;

        MECONF.LAYERS = function () {
            var mapbox = L.tileLayer(
                    'http://api.tiles.mapbox.com/v4/rparra.jmk7g7ep/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicnBhcnJhIiwiYSI6IkEzVklSMm8ifQ.a9trB68u6h4kWVDDfVsJSg');
            var osm = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {minZoom: 3});
            var gglHybrid = new L.Google('HYBRID');
            var gglRoadmap = new L.Google('ROADMAP');
            return {
                MAPBOX: mapbox,
                OPEN_STREET_MAPS: osm,
                GOOGLE_HYBRID: gglHybrid,
                GOOGLE_ROADMAP: gglRoadmap
            }
        };

        var finishedLoading = function() {
          if(tilesLoaded){
            $(".spinner").remove();
            MECONF.tilesLoaded = false;
          }

        };

        var startLoading = function() {
          var spinner = new Spinner({
              color: "#ffb885",
              radius: 10,
              width: 5,
              length: 10,
              top: '92%',
              left: '98%'
          }).spin();
          $("#map2").append(spinner.el);
        };

        var setup_gmaps = function() {
          google.maps.event.addListenerOnce(this._google, 'tilesloaded', tilesLoaded);
        };

        var tilesLoaded = function(){
            MECONF.tilesLoaded = true;
            finishedLoading();
          }

        startLoading();

        L.mapbox.accessToken = 'pk.eyJ1IjoicnBhcnJhIiwiYSI6IkEzVklSMm8ifQ.a9trB68u6h4kWVDDfVsJSg';
        var layers = MECONF.LAYERS();
        var mapbox = layers.MAPBOX.on('load', tilesLoaded);
        var osm = layers.OPEN_STREET_MAPS.on('load', tilesLoaded);

        var gglHybrid = layers.GOOGLE_HYBRID.on('MapObjectInitialized', setup_gmaps);
        var gglRoadmap = layers.GOOGLE_ROADMAP.on('MapObjectInitialized', setup_gmaps);

        /* Layer para probar opacidad */
        /*var historic_seattle = new L.tileLayer.wms('http://demo.lizardtech.com/lizardtech/iserv/ows', {
                layers: 'Seattle1890',
                maxZoom: 18,
                format: 'image/png',
                transparent: true
            });*/

        var map = L.map('map', {maxZoom: 18, minZoom: 3, worldCopyJump: true, attributionControl: false, zoomControl: false})
              .setView([-24, -57.189], 7)
              //.setView([47.59, -122.30], 12)
              .on('baselayerchange', startLoading);
        new L.Control.Zoom({ position: 'topright' }).addTo(map);

        var baseMaps = {
          'Calles OpenStreetMap': osm,
          'Terreno': mapbox,
          'Satélite': gglHybrid,
          'Calles Google Maps': gglRoadmap
        };

        //map.addLayer(gglRoadmap);

        var departamentos;

        mapaFactory.getDepartamentos().then(function(data){
          departamentos = L.geoJson(data,  {onEachFeature: mapaUtil.onEachFeature}).addTo(map);
          mapaUtil.setGeoJsonLayer(departamentos);
          return departamentos;
        }).then(function(data){
          return mapaUtil.init(1, 1, data);
        }).then(function(data){
          console.log(data);
          mapaUtil.getRiesgosLegend().addTo(map);
          mapaUtil.getRiesgoInfo().addTo(map);
          data.setStyle(mapaUtil.getStyle);
        });

        mapaUtil.setMap(map);
        
        /*mapaFactory.getDepartamentosFeature().then(function(data){
          console.log('luego de obtener departamentos');
          departamentos = L.geoJson(data,  {style: getStyle, onEachFeature: onEachFeature}).addTo(map);
          console.log(departamentos);
        });

        function onEachFeature(feature, layer) {
          layer.on({
              mouseover: mouseover,
              mouseout: mouseout,
              click: zoomToFeature
          });

        }

        function mouseover(e) {
            var layer = e.target;
             layer.setStyle({
                weight: 5,
                color: '#666',
                dashArray: ''

            });

            if (!L.Browser.ie && !L.Browser.opera) {
                layer.bringToFront();
            }
        }

        function mouseout(e) {
            departamentos.resetStyle(e.target);
            var layer = e.target;
             layer.setStyle({
                weight: 2,
                color: '#666',
                dashArray: ''

            });

        }

        function zoomToFeature(e) {    

        }

        function getColor(d) {
            return d == 'E' ? '#FE0516' :
                d == 'RA' ? '#FF6905' :
                d == 'RM' ? '#FFB905' :
                '#FFF96D';
        }

        function getStyle(feature) {
            var n = feature.properties.dpto_desc;
            var color = "#000";
            return { weight: 2,
                  opacity: 5,
                  color: '#666',
                  //dashArray: '3',
                  fillOpacity: 0.5,
                  fillColor: getColor(color)
              };

        }*/

      }
    }
  });


