'use strict';

/**
 * @ngdoc overview
 * @name capacitacionApp
 * @description
 * # capacitacionApp
 *
 * Main module of the application.
 */
angular
  .module('capacitacionApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/tags', {
        templateUrl: 'views/tags.html',
        controller: 'TagsCtrl',
        controllerAs: 'tags'
      })
      .when('/graph', {
        templateUrl: 'views/graph.html',
        controller: 'GraphCtrl',
        controllerAs: 'graph'
      })
      .when('/bubbles', {
        templateUrl: 'views/bubbles.html',
        controller: 'BubblesCtrl',
        controllerAs: 'bubbles'
      })
      .when('/mapD3', {
        templateUrl: 'views/mapd3.html',
        controller: 'MapCtrl',
        controllerAs: 'map'
      })
      .when('/mapLeaflet', {
        templateUrl: 'views/map.html',
        controller: 'MapCtrl',
        controllerAs: 'map2'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
