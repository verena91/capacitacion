'use strict';

/**
 * @ngdoc function
 * @name capacitacionApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the capacitacionApp
 */
angular.module('capacitacionApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
