'use strict';

/**
 * @ngdoc function
 * @name capacitacionApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the capacitacionApp
 */
angular.module('capacitacionApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
