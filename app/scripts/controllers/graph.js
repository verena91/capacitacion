'use strict';

/**
 * @ngdoc function
 * @name capacitacionApp.controller:GraphCtrl
 * @description
 * # GraphCtrl
 * Controller of the capacitacionApp
 */
angular.module('capacitacionApp')
  .controller('GraphCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    console.log('graph');
  });
