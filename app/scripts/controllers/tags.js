'use strict';

/**
 * @ngdoc function
 * @name capacitacionApp.controller:TagsCtrl
 * @description
 * # TagsCtrl
 * Controller of the capacitacionApp
 */
angular.module('capacitacionApp')
  .controller('TagsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    console.log('tags cloud');
  });
