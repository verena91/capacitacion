'use strict';

/**
 * @ngdoc function
 * @name capacitacionApp.controller:BubblesCtrl
 * @description
 * # BubblesCtrl
 * Controller of the capacitacionApp
 */
angular.module('capacitacionApp')
  .controller('BubblesCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    console.log('bubbles');
  });
