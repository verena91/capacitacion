'use strict';

/**
 * @ngdoc function
 * @name capacitacionApp.controller:MapCtrl
 * @description
 * # MapCtrl
 * Controller of the capacitacionApp
 */
angular.module('capacitacionApp')
  .controller('MapCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    console.log('map');
  });
