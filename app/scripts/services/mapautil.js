'use strict';



angular.module('capacitacionApp')
  .factory('mapaUtil', function(mapaFactory, $q) {
  		return {
  			onEachFeature: onEachFeature,
  			reloadAnio: reloadAnio,
  			getRiesgosLegend: getRiesgosLegend,
  			getRiesgoInfo: getRiesgoInfo,
  			reloadSem: reloadSem,
  			getStyle: getStyle,
  			setGeoJsonLayer: setGeoJsonLayer,
  			init: init,
  			setMap: setMap
  		};

  		var departamentosRiesgo;
		var distritosRiesgo;
		var asuncionRiesgo;
		var riesgoSemanaDep;
		var riesgoSemanaDis;
		var semana;
		var anio;
		var info;
		var geoJsonLayer;
		var drillDown = false;
		var map;
		var layerActual;
		var inZoom = false;

		function setMap(mapa){
			map = mapa;
		}
		function setGeoJsonLayer(data){
			geoJsonLayer = data;
		}

		function reloadSem(semana){

		    var rasu;
		    var rcen;
		    var mapSem = new Object();
		    var mapSemDis = new Object();

		    //Cargar el json de los riesgos por semana
		    //riesgos de todas las semanas de los departamentos
		    var riesgo = departamentosRiesgo;
		    //riesgos de todas las semana de los distritos
		    var riesgoDis = distritosRiesgo;
		    //riesgos de todas las semanas de los barrios de asuncion
		    //var riesgoAsu = asuncionRiesgo;
		    console.log("reloadSem - - -");
		    console.log(riesgo);
		    console.log(riesgoDis);
		    for(var i=0; i<riesgo.length; i++){
		        var obj = riesgo[i];
		        //if(obj["semana"]==semana ){
		            mapSem[obj["valor"]]= obj;
		            /*if(obj["departamento"]=="CENTRAL"){
		                rcen = obj;
		            }
		            if(obj["departamento"]=="ASUNCION"){
		                rasu = obj;
		            }*/
		        //}
		    }
		    /*if(rasu){
		       try{
		            rasu["riesgo"] = rcen["riesgo"];
		        }catch(e){
		            rasu["riesgo"] = 'RB';
		        }
		        mapSem["ASUNCION"] = rasu;
		    }*/
		    //Riesgo de los departamentos de la semana seleccionada
		    riesgoSemanaDep = mapSem;

		    for(var i=0; i<riesgoDis.length; i++){
		        var obj = riesgoDis[i];
		        //if(obj["semana"]==semana ){

		            mapSemDis[obj["valor"]]= obj;
		            /*if(obj["departamento"]=="CENTRAL"){
		                rcen = obj;
		            }
		            if(obj["distrito"] == "ASUNCION"){
		                //rasu = obj;
		            }*/
		        //}
		    }
		    //Incluir en el mapSemDis los barrios de Asuncion
		    /*for(var i=0; i<riesgoAsu.length; i++){
		        var obj = riesgoAsu[i];
		        if(obj["semana"] == semana){
		            mapSemDis[obj["barrio"]] = obj;
		        }

		    }*/
		   	//Riesgos de los distritos de la semana seleccionada
		    riesgoSemanaDis = mapSemDis;
		    geoJsonLayer.setStyle(getStyle);
		}

  		function init(sem, ano, layer){
  			var defered = $q.defer();
      		var promise = defered.promise;
  			semana = sem;
  			anio = ano;
  			return mapaFactory.getCantidadesDepartamentos(anio)
  				.then(function(data){
    				departamentosRiesgo =  data;
    				return mapaFactory.getCantidadesDistritos(anio);
    			})
  				.then(function(data){
  					console.log("riesgos de distritos- ");
  					distritosRiesgo = data;
  					reloadSem(semana);
  					defered.resolve(layer);
  					return promise;
  				})
    			.catch(function(err){
    				console.log(err);
    			});
  		}

  		function reloadAnio(sem, ano){
  			semana = sem;
  			anio = ano;
  			mapaFactory.getCantidadesDepartamentos(anio)
  				.then(function(data){
    				departamentosRiesgo =  data;
    				return mapaFactory.getCantidadesDistritos(anio);
    			})
  				.then(function(data){
  					console.log("riesgos de distritos- ");
  					distritosRiesgo = data;
  					reloadSem(semana);

  				})
    			.catch(function(err){
    				console.log(err);
    			});
  		}

  		function getRiesgosLegend(){
  			var riesgosLegend = L.control({
		        position: 'bottomright'
		    });
		    riesgosLegend.onAdd = function(map) {
		        var div = L.DomUtil.create('div', 'info legend'), labels = [];
		        labels.push('<i style="background:' + getColor(0) + '"></i> ' + '0 solicitudes');
		        labels.push('<i style="background:' + getColor(1) + '"></i> ' + '1 - 10 solicitudes');
		        labels.push('<i style="background:' + getColor(11) + '"></i> ' + '11 o mas solicitudes');
		        div.innerHTML = '<span>Umbrales de riesgo</span><br>' + labels.join('<br>')
		        return div;
		    };
		    return riesgosLegend;
  		}

  		function getRiesgoInfo(){
			info = L.control();
		    info.onAdd = function (map) {
		        this._div = L.DomUtil.create('div', 'info');
		        this.update();
		        return this._div;
		    };
		    info.update = function (props) {

		        if(props){
		        	console.log(props);
		            var dep = props.departamento;
		            //if(SMV.onriesgo){
		                var mapSem = riesgoSemanaDep;
		            /*}else{
		                var mapSem = SMV.mapSemFil;
		            }*/
		            console.log(riesgoSemanaDep);
		            //console.log()
		            var nroNS = '0';
		            try{
		                nroNS = mapSem[dep]["cantidad"];
		            }catch(e){

		            }
		          this._div.innerHTML =  '<h2>Dpto: '+dep+'<\/h2><h2>Solicitudes: '+nroNS+'<\/h2>';
		        }
		    };
		    info.updateDrillDown = function (props){

		        if(props){

		            var dep = props.first_dp_1;
		            var depAsu = props.departamento;
		            var dis = props.first_di_1;
		            var mapSem = riesgoSemanaDis;
		            var nroNS = '0';

		            var info;
		            /*if(depAsu == 'ASUNCION'){
		            	dis = props.dist_desc;
		                var key = depAsu+'-'+props.barlo_desc;
		                try{
		                    nroNS = mapSem[key]["cantidad"];
		                }catch(e){

		                }
		                info = '<h2>Dpto: '+depAsu+'<\/h2><h2>Distrito: '+dis+'<\/h2><h2>Barrio: '+props.barlo_desc+'<\/h2><h2>Solicitudes: '+nroNS+'<\/h2>';
		            } else {*/
		                var key = dep+'-'+dis;
		                try{
		                    nroNS = mapSem[key]["cantidad"];
		                }catch(e){

		                }
		                info = '<h2>Dpto: '+dep+'<\/h2><h2>Distrito: '+dis+'<\/h2><h2>Solicitudes: '+nroNS+'<\/h2>';
		            //}
		          this._div.innerHTML =  info;
		        }
	    	};
	    	return info;
	    }

		function onEachFeature(feature, layer) {
		    layer.on({
		        mouseover: mouseover,
		        mouseout: mouseout,
		        click: zoomToFeature
		    });

		}

		function mouseover(e) {
		    var layer = e.target;
		     layer.setStyle({
		        weight: 5,
		        color: '#666',
		        dashArray: ''

		    });

		    if (!L.Browser.ie && !L.Browser.opera) {
		        layer.bringToFront();
		    }
		    info.update(layer.feature.properties);
		}

		/*Evento al salir el puntero de un departamento*/
		function mouseout(e) {
		    //geoJsonLayer.resetStyle(e.target);
		    var layer = e.target;
		     layer.setStyle({
		        weight: 2,
		        color: '#FFF',
		        dashArray: ''

		    });
		    info.update();

		}

		/*Zoom al hacer click en un departamento*/
		function zoomToFeature(e) {
			drillDown = true;
		    var target = e.target;
		    //pedir distritos?
		    console.log(target.feature.properties.departamento);
		    /*if(target.feature.properties.departamento == 'ASUNCION'){
		    	 mapaFactory.getBarrios(target.feature.properties.departamento)
		    	.then(function(data){
    				var json = data;
    				inZoom = true;
    				geoJsonLayer.setStyle(getStyle);
				    if(layerActual){
				        map.removeLayer(layerActual);
				    }
				    layerActual = L.geoJson(json,  {style: getStyleDrillDown, onEachFeature: onEachFeatureDrillDown}).addTo(map);
				    map.fitBounds(target.getBounds());
				    console.log(data);
    			});
		    } else {*/
		    	mapaFactory.getDistritos(target.feature.properties.departamento)
		    	.then(function(data){
    				var json = data;
    				inZoom = true;
    				geoJsonLayer.setStyle(getStyle);
				    if(layerActual){
				        map.removeLayer(layerActual);
				    }
				    layerActual = L.geoJson(json,  {style: getStyleDrillDown, onEachFeature: onEachFeatureDrillDown}).addTo(map);
				    map.fitBounds(target.getBounds());
				    console.log(data);
    			});
		    //}
		   
		    
		    /*if(!SMV.inzoom){
		        var button = new SMV.backClass();
		        SMV.map.addControl(button);
		        SMV.backButton = button;
		    }*/
		

		}

    	function getColor(d) {
		    return d > 10 ? '#FE0516' :
		        d > 0 && d < 10 ? '#FF6905' :
		        '#FFF96D';
		}

		/*Estilo de la capa de departamento de acuedo a los valores de riesgo*/
		function getStyle(feature) {
			console.log(feature);
		    var n = feature.properties.departamento;
		    var mapSem = riesgoSemanaDep;
		    var color = 'NONE';
		    try{
		        color = mapSem[n]["cantidad"];
		    }catch(e){
		    }
		    if(inZoom){
		    	return { weight: 2,
		            opacity: 5,
		            color: 'white',
		            dashArray: '3',
		            fillOpacity: 0,
		            fillColor: getColor(color)
		        };
		    }
		  	else{
		  		return { weight: 2,
		            opacity: 5,
		            color: 'white',
		            dashArray: '3',
		           	fillOpacity: 0.5,
		            fillColor: getColor(color)
		        };
		  	}

		}

		/*Estilo de la capa de distritos de acuedo a los valores de riesgo*/
		function getStyleDrillDown(feature) {
		    var prop = feature.properties;
		    var n = prop.first_dp_1+'-'+prop.first_di_1;
		    var mapSem = riesgoSemanaDis;
		    var color = 'NONE';
		    /*if(prop.departamento == 'ASUNCION'){
		        n = prop.departamento+'-'+prop.barlo_desc;
		    }*/
		   try{
		        color = mapSem[n]["riesgo"];
		       
		    }catch(e){
		    }
		   
		    return { weight: 2,
		            opacity: 1,
		            color: 'white',
		            dashArray: '3',
		            fillOpacity: 0.5, 
		            fillColor: getColor(color) 
		        };
		}
		/*Eventos para cada distrito*/
		function onEachFeatureDrillDown(feature, layer) {
		    layer.on({
		        mouseover: mouseoverDrillDown,
		        mouseout: mouseoutDrillDown,
		    });
		}
		/*Evento al ubicarse el puntero sobre un distrito*/
		function mouseoverDrillDown(e) {
		    var layer = e.target;
		     layer.setStyle({
		        weight: 5,
		        color: '#666',
		        dashArray: ''
		        
		    });

		    if (!L.Browser.ie && !L.Browser.opera) {
		        layer.bringToFront();
		    }
		    info.updateDrillDown(layer.feature.properties);
		}
		/*Evento al salir el puntero de un distrito*/
		function mouseoutDrillDown(e) {
		    layerActual.resetStyle(e.target);
		    info.updateDrillDown();
		   
		}
});