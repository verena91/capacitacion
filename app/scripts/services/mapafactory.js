'use strict';

/**
 * @ngdoc function
 * @name capacitacionApp:mapaFactory
 * @description
 * # mapaFactory
 * Factory in the capacitacionApp
 */
angular.module('capacitacionApp')
  .factory('mapaFactory', function($http, $q) {
  		// usamos topojson de departamentos y distritos publicados por salud
  		//var urlBase = "http://www.mspbs.gov.py/api/rest";


  		return {
  			getDepartamentos: function(parametro){
				var req = {
					method: 'GET',
					dataType: "json",
				    url: 'data/map-data/topojson_visualizacion.json',
				    //params: parametro,
				    headers: {
				        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				    }
				};
				return $http(req).then(function(result){
					//console.log(result.data);
					return topojson.feature(result.data, result.data.objects["visualizacion"]);
				});

			},

			getDistritos: function(depto){
				console.log('aaa');
				console.log(depto);
		        var nombreDepto = depto.toLowerCase().replace(' ', '_');
		        var path = 'topojson_' + nombreDepto + '.json';
		        var req = {
		          method: 'GET',
		          dataType: "json",
		            url: 'data/map-data/distritos/' + path,
		            //params: parametro,
		            headers: {
		                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		            }
		        };
		        return $http(req).then(function(result){
		          //console.log(result.data);
					return topojson.feature(result.data, result.data.objects[nombreDepto]);
		        });

			},

			getCantidadesDepartamentos: function(depto){
		        var req = {
		          method: 'GET',
		          dataType: "json",
		            url: 'data/map-data/data.json',
		            //params: parametro,
		            headers: {
		                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		            }
		        };
		        return $http(req).then(function(result){
		          return result.data;
		        });

			},
			
			getCantidadesDistritos: function(){
		        var req = {
		          method: 'GET',
		          dataType: "json",
		            url: 'data/map-data/data2.json',
		            //params: parametro,
		            headers: {
		                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		            }
		        };
		        return $http(req).then(function(result){
		          return result.data;
		        });

			},

			getDepartamentosFeature: function(){
		  		var defered = $q.defer();
		  		var promise = defered.promise;
		      	var url = 'http://localhost:8080/geoserver/sf/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=sf:mapa_departamento_paraguay&outputFormat=text/javascript&format_options=callback:JSON_CALLBACK';
		  		$http.jsonp(url, {cache: true})
		  			.success(function(data){
		  				defered.resolve(data);
		  			})
		  			.error(function(err){
		  				defered.reject(err);
		  			});
		  		return promise;	
		  	}
  		} //return

});
